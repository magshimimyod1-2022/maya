from collections import Counter
import requests
def extract_password_from_site():
    #The function finds the secret password.
    URL = "http://webisfun.cyber.org.il/nahman/files/"
    response = requests.get(URL)
    html = response.text
    password = ""
    for i in range(11, 35):
        link = "http://webisfun.cyber.org.il/nahman/files/file%d.nfo" % i
        opened = requests.get(link)
        html_file = opened.text
        html_list = list(html_file)
        password += html_list[99]
    print(password)

def find_most_common_words(file_path, num_of_words):
    """The function gets a file path and a certain number of the most common words to print.
    The function will find the most common words using the function Counter.
    :param file_path: the file path.
    :param num_of_words: the amount of the most common words to print.
    :type file_path: string.
    :type num_of_words: int.
    :return: none.
    """
    with open(file_path, 'r') as opened_file:
        opened_file = opened_file.read()
        list_file = list(opened_file.split(" "))
        most_common_words = [word for word, word_count in Counter(list_file).most_common(num_of_words)]
        most_common_words_str = ""
        for word in most_common_words:
          most_common_words_str += word
          most_common_words_str += " "
        print(most_common_words_str)

def main():
    choise = int(input("Enter your choise(1 = password, 2 = sentence): "))
    if choise == 1:
        extract_password_from_site()
    elif choise == 2:
        find_most_common_words("words.txt", 6)
    else:
        print("invalid choise...")
        main()

if __name__ == "__main__":
    main()

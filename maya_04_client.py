import socket
import keyboard

SERVER_IP = "127.0.0.1"
SERVER_PORT = 21623


def create_client():
    """
    The function connect to the server, prints to the user the options they have and prints out
    the answer from the server.
    :return: none.
    """
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        # connecting to the server.
        server_address = (SERVER_IP, SERVER_PORT)
        sock.connect(server_address)
        # receiving the welcome message from the server and printing it.
        from_server_msg = sock.recv(1024)
        from_server_msg = from_server_msg.decode()
        print(from_server_msg)
        user_choice = 0
        # creating the dictionary of messages to the server.
        to_server_dict = {
            1: "option1;",
            2: "option2;user_input=",
            3: "option3;user_input=",
            4: "option4;user_input=",
            5: "option5;user_input=",
            6: "option6;user_input=",
            7: "option7;user_input=",
            8: "option8;exit"}
        while user_choice != 8:
            print("Hello!\nWelcome to the pink floyd program:)\nHere are your options:\n")
            print("1. List of albums.\n2. Get the songs of an album of your choice.\n")
            print("3. Get the length of a song of your choice.\n4. Get the lyrics of a song of your choice. \n")
            print("5. Get the album name of a song of your choice.\n")
            print("6. Get all the song names that contain your input word.\n")
            print("7. Get all the song names that contain your input word in their lyrics.\n8. Exit the program.\n")
            user_choice = int(input("What is your choice? "))
            while not (1 <= user_choice <= 8):
                user_choice = int(input("Invalid choice! Try again: \n"))
            # sending the message to the server.
            message_to_server = to_server_dict[user_choice]
            sock.sendall(message_to_server.encode())
            # waiting for a response from the server.
            server_answer = sock.recv(1024)
            server_answer = server_answer.decode()
            print(server_answer)
            if user_choice == 8:
                exit()
            if keyboard.ispressed("Ctrl+c"):
                exit()
        if user_choice == 8:
            exit()


def main():
    create_client()


if __name__ == "__main__":
    main()

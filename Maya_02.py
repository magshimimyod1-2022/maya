import datetime
import socket
def find_weather(city_name, date):
    """
    The function receives the name of the city and country and the date, sends them to the server and
    receives the weather and the description.
    :param city_name: The name of the city and country.
    :param date: Today's date.
    :type city_name: string.
    :type date: string.
    :return: A tuple with the weather and a description of the weather.
    :rtype: tuple.
    """
    only_city = city_name.split(",")[0]
    only_city= only_city.lower()
    SERVER_IP = "34.218.16.79"
    SERVER_PORT = 77
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = (SERVER_IP, SERVER_PORT)
    sock.connect(server_address)
    checksum = calc_checksum(city_name, date)
    to_server_msg = "100:REQUEST:city="+only_city+"&date="+date+"&checksum="+checksum
    sock.sendall(to_server_msg.encode())
    from_server_msg = sock.recv(1024)
    from_server_msg = from_server_msg.decode()
    from_server_msg = sock.recv(1024)
    is_error = from_server_msg[0:3]
    if is_error == b'200':
        find_temp = from_server_msg.find(b'temp')
        find_temp += 5
        temperature = from_server_msg[find_temp: (find_temp+5)]
        temperature = temperature.decode("utf-8")
        find_description = from_server_msg.find(b'text')
        find_description += 5
        description = from_server_msg[find_description:]
        description = description.decode("utf-8")
        weather = (temperature, description)
        return weather
    else:
        print("999\n Unknown city.")

def calc_checksum(city_name, date):
    """
    The function calculated the checksum by removing the country from city_name and the "/" from the date.
    Afterwards, converts the city name to ascii and summarize the number's values of the letter.
    Also summarizes the numbers of the date.
    :param city_name: The name of the city and country.
    :param date: The date.
    :type city_name: String.
    :type date: String.
    :return: The checksum.
    :rtype: String.
    """
    only_city = city_name.split(",")[0]
    only_city= only_city.lower()
    ascii_city = [ord(character) - 96 for character in only_city]
    count_city = 0
    for num in ascii_city:
        count_city += (num)
    date = date.replace("/", "")
    count_date = 0
    for num in date:
        count_date += int(num)
    str_date = str(count_date)
    str_city = str(count_city)
    checksum = str_city+"."+str_date
    return checksum

def main():
    city_name = input("Enter name of city and country (Paris, France): ")
    today = datetime.date.today()
    date = today.strftime("%d/%m/%Y")
    weather = find_weather(city_name, date)
    print("Hello! Welcome to weather client!\nHere are your options:\n")
    choise = int(input("1. The weather today.\n2.The weather today and in the next three days.\n"))
    if choise == 1:
         print(weather)
    elif choise == 2:
        for i in range(4):
            today = datetime.date.today()+datetime.timedelta(days=i)
            date = today.strftime("%d/%m/%Y")
            weather = find_weather(city_name, date)
            print(date+", Temperature: " + weather[0] + ", " +weather[1])
    else:
        print("Invalid choise...")


if __name__ == "__main__":
    main()

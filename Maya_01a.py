def physic_ok(behavior):
  #gets the behavior in question.
    #inputs necessary data and combines it in a print sentence- inputs something.
    physic = input("What is the physical equivalent of {}?\n".format(behavior))
    physic_op = input('Do you consider the behavior "{}" ok? please write "yes" or "no"\n'.format(physic))
    return physic_op.lower() == 'yes'


def beneficial(behavior):
    #the function checks how many people will it harm/ benefit.
    #inputs how many people will the action harm and based on that asks a certain question.
    num_harmed = int(input("How many people will be harmed if you do it?\n"))
    harm_level = 0
    #if it harms less than 5 people:
    if num_harmed < 5:
        #range 1 - num_harmed + 1
        for person in range(1, num_harmed + 1):
            #inputs the level of harm.
            level = input(
                "What level of harm will be made to the {}st person? please enter a number from 1 to 10\n".format(person))
            harm_level += int(level)
    #if it harms more than 5 people.
    else:
        harm_level = int(input("what average level of harm is done to each person? please enter a number from 1 to 10\n")) * num_harmed

    num_benefit = int(input("How many people will benefit if you do it?\n"))
    good_level = 0
  #if it benefits less than 5 people:
    if num_benefit < 5:
      #checks how beneficial is it for each person.
        for person in range(1, num_benefit):
            good = input(
                "What level of benefit will be made to the {}st person? please enter a number from 1 to 10\n".format(person))
            good_level += int(good)
    #if it benefits more than 5 people: checks the level of beneficiality.
    else:
        good_level = int(input("what average level of benefit will be made to each person? please enter a number from 1 to 10\n")) * num_benefit

    return good_level > harm_level


def main():
  #prints the first and the last messages.
    behavior = input("Welcome to the moral decision making assistant! What is the behavior in question?\n")
    physic_answer = physic_ok(behavior)
  #if they dont consider the behavior ok:
    if not(physic_answer):
        print ("According to our analysis, you should not to it. Try to find other ways to solve your problem.")
      #otherwise, calls another function that asks more questions.
    else:
        beneficent = beneficial(behavior)
        if beneficent:
          #my addicion:
            family_harm = int(input("How many families will the action harm? "))
            family_benefit = int(input("How many families will it benefit? "))
            diff = abs(family_harm - family_benefit)
            if family_harm < family_benefit and diff > 5:
              print ("According to our analysis, you should do it.\nHave a great day!")
            else:
              print ("According to our analysis, you should not to it. Try to find other ways to solve your problem.")


if __name__ == "__main__":
    main()

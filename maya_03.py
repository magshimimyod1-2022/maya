import keyboard
from tkinter import *
from tkinter import messagebox
#from gettext import find
#from http import server
import socket
def create_proxy():
    SERVER_MOVIE_PORT = 92
    MOVIE_IP = "54.71.128.194"
    SERVER_PORT = 9090
    client_address = "127.0.0.1"
    while(keyboard.ispressed("Ctrl+Break") == False):
        with socket.socket() as listening_sock:
            #creating socket
            listening_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            #listen to port 9090.
            server_address = ('', SERVER_PORT)
            listening_sock.bind(server_address)
            listening_sock.listen(1)
            #conversation socket
            client_soc, client_address = listening_sock.accept()
            client_msg = client_soc.recv(1024)
            client_msg = client_msg.decode()
            print(client_msg)
            with socket.socket() as sock:
                sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                server_address1 = (MOVIE_IP, SERVER_MOVIE_PORT)
                sock.connect(server_address1)
                sock.sendall(client_msg.encode())
                server_msg = sock.recv(1024)
                server_msg = server_msg.decode()
                new_msg = ""
                country = (client_msg.find("y:"))
                country = client_msg[country + 2: ]
                if(country == 'France'):
                    server_msg = 'ERROR#"No movie found"'
                if ('ERROR' not in server_msg):
                    find_ep = (server_msg.index('&'))
                    part1_msg = server_msg[0: find_ep - 2]
                    part2_msg = server_msg[find_ep - 1: ]
                    proxy_name = " proxy"
                    new_msg = part1_msg + proxy_name + part2_msg
                    #fixing the picture problem; there was a . missing before the jpg.
                    find_jpg = (new_msg.index('&id'))
                    part1_pic = new_msg[0: find_jpg - 4]
                    part2_pic = new_msg[find_jpg - 4: ]
                    point = "."
                    new_msg_pic = part1_pic + point + part2_pic
                    print(new_msg_pic)
                
                elif ('ERROR' in server_msg and country == 'France'):
                    print("France is banned!")
                    top = Tk()  
                    top.geometry("100x100")  
                    messagebox.showwarning("warning","France is banned!")
                
                else:
                    print("Program issues... movie not found.")
            client_soc.sendall(server_msg.encode())
    if keyboard.ispressed("Ctrl+Break"):
        exit()
            

def main():
    create_proxy()


if __name__ == "__main__":
    main()
